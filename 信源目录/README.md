# 声明：

1.我们会在这里长期更新有用的站点信息源。

2.如果你有需要查找的信息源，请告诉我们，我们会第一时间跟你反馈。

3.如果你有对大家有帮助的信息源，也请告诉我们，把有价值的信息源跟大家分享！


- 如有任何问题请联系经理

   **微信号： javabloger | 电话： 13913853100** 

<img src="https://gitee.com/stonedtx/yuqing/raw/master/ProIMG/%E8%81%94%E7%B3%BB%E6%88%91%E4%BB%AC-%E4%B8%AA%E4%BA%BA%E5%BE%AE%E4%BF%A1.jpg" title="Logo"  width="180">

<br><br>

# 信源目录


## 招标信源

[主流招投标信源列表](https://gitee.com/stonedtx/open-spider/blob/master/%E4%BF%A1%E6%BA%90%E7%9B%AE%E5%BD%95/%E6%8B%9B%E6%A0%87%E4%BF%A1%E6%BA%90/master-Tender.md
)

[省级政府招投标信源列表](https://gitee.com/stonedtx/open-spider/blob/master/%E4%BF%A1%E6%BA%90%E7%9B%AE%E5%BD%95/%E6%8B%9B%E6%A0%87%E4%BF%A1%E6%BA%90/province-Tender.md)


[市级政府招投标信源列表]

[区县级政府招投标信源列表]

[高校招投标信源列表](https://gitee.com/stonedtx/open-spider/blob/master/%E4%BF%A1%E6%BA%90%E7%9B%AE%E5%BD%95/%E6%8B%9B%E6%A0%87%E4%BF%A1%E6%BA%90/university.md) 

[银行招投标信源列表] 

[大型企业招投标信源] 

[电信运营商招投标信源](https://gitee.com/stonedtx/open-spider/blob/master/%E4%BF%A1%E6%BA%90%E7%9B%AE%E5%BD%95/%E6%8B%9B%E6%A0%87%E4%BF%A1%E6%BA%90/ISP-Tender.md)

## 主流媒体


## 报刊媒体


## 金融证券


## 政府网站


## 电商相关


## 汽车相关



