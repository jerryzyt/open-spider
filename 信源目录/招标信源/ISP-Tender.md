## 运营商招标信源站点

### 更新日期：2022-3-15 

- 如有任何问题请联系经理

   **微信号： techflag | 电话： 13505146123** 

<img src="https://gitee.com/stonedtx/yuqing/raw/master/ProIMG/%E8%81%94%E7%B3%BB%E6%88%91%E4%BB%AC-%E4%B8%AA%E4%BA%BA%E5%BE%AE%E4%BF%A1.jpg" title="Logo"  width="180">

<br><br>

1.中国移动	https://b2b.10086.cn/b2b/main/preIndex.html

2.中国联通	http://www.chinaunicombidding.cn/jsp/cnceb/web/forword.jsp

3.中国电信	https://caigou.chinatelecom.com.cn/MSS-PORTAL/account/login.do

4.中国铁塔	http://www.tower.com.cn/