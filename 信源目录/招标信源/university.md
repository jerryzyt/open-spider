## 全国高校招标信源站点

### 更新日期：2022-3-16

- 如有任何问题请联系经理

   **微信号： techflag | 电话： 13505146123** 

<img src="https://gitee.com/stonedtx/yuqing/raw/master/ProIMG/%E8%81%94%E7%B3%BB%E6%88%91%E4%BB%AC-%E4%B8%AA%E4%BA%BA%E5%BE%AE%E4%BF%A1.jpg" title="Logo"  width="180">

<br><br>

北京大学	https://www.lab.pku.edu.cn		北京大学实验室与设备管理部

北京大学	https://zwb.pku.edu.cn		北京大学总务部

北京大学	http://lab.bjmu.edu.cn		招标采购_北京大学医学部设备与实验室管理处

中国人民大学	http://cgzx.ruc.edu.cn		中国人民大学采购与招标管理中心

中国人民大学	http://www.ruc.edu.cn		中国人民大学

清华大学	http://sbcgczxxfb.sysc.tsinghua.edu.cn	清华大学设备采购信息发布平台

清华大学	http://hq.tsinghua.edu.cn		清华大学后勤综合信息

清华大学	https://www.tsinghua.edu.cn		清华大学

北京交通大学	http://gzc.bjtu.edu.cn		北京交通大学-国有资产管理处

北京工业大学	http://xxgk.bjut.edu.cn		北京工业大学-信息公开网

北京工业大学	http://whcg.bjut.edu.cn		北京工业大学—化学品采购管理平台

北京航空航天大学	http://zbcg.buaa.edu.cn		北京航空航天大学招标采购管理中心

北京航空航天大学	http://asset.buaa.edu.cn  北航空校园规划建管理中心

北京理工大学	http://zczx.bit.edu.cn		北京理工大学采购管理系统

北京科技大学	http://xxgk.ustb.edu.cn		北京科技大学党务公开、信息公开网

北京科技大学	https://zc.ustb.edu.cn		北京科技大学资产管理处

北京化工大学	https://cgb.buct.edu.cn		北京化工大学采购与招标办公室

北京化工大学	https://cg.buct.edu.cn		北京化工大学采购服务平台

北京工商大学	https://zcc.btbu.edu.cn		北京工商大学国有资产管理处

北京工商大学	https://xyjsc.btbu.edu.cn		北京工商大学校园建设处

北京邮电大学	https://cg.bupt.edu.cn		北京邮电大学采购与招标办公室

北京印刷学院	http://zcc.bigc.edu.cn		北京印刷学院国有资产管理处

中国农业大学	http://cg.cau.edu.cn		中国农业大学招标采购与资产管理系统

北京中医药大学	http://zcglc.bucm.edu.cn		北京中医药大学资产管理处

北京师范大学	https://cg.bnu.edu.cn		北京师范大学采购管理平台

北京师范大学珠海校区	https://ztb.bnuz.edu.cn	北京师范大学珠海校区招标

北京师范大学	http://jijianchu.bnu.edu.cn		北京师范大学基建处

首都师范大学	https://gzc.cnu.edu.cn		首都师范大学国有资产管理处

北京外国语大学	https://zczb.bfsu.edu.cn	北京外国语大学招投标管理中心

北京语言大学	http://cgb.blcu.edu.cn		北京语言大学采购与招标管理办公室

北京语言大学	http://jijian.blcu.edu.cn		北京语言大学基建处

北京语言大学	http://zcglc.blcu.edu.cn		北京语言大学资产管理处

对外经济贸易大学	http://zhaobiao.uibe.edu.cn		对外经济贸易大学采购网

北京物资学院	http://zcc.bwu.edu.cn		北京物资学院资产管理处

北京物资学院	https://zcc.bwu.edu.cn		北京物资学院资产管理处

首都经济贸易大学	https://cwc.cueb.edu.cn		首都经济贸易大学-财务处

中央美术学院	http://cg.cafa.edu.cn		中央美术学院采购管理系统

北京舞蹈学院	https://gzc.bda.edu.cn		北京舞蹈学院_国有资产处

中国政法大学	http://ztbjcgglbgs.cupl.edu.cn		

中国政法大学招投标及采购管理办公室

华北电力大学	https://zb.ncepu.edu.cn		华北电力大学招标中心

华北电力大学	https://zbzx.ncepu.edu.cn		华北电力大学招标中心

北京信息科技大学	https://houqin.bistu.edu.cn		北京信息科技大学——后勤管理处

中国矿业大学（北京）	http://zcc.cumtb.edu.cn		中国矿业大学资产管理处

中国地质大学（北京）	http://cggl.cug.edu.cn		中国地质大学招标管理中心

北京财贸职业学院	http://cg.bjczy.edu.cn		北京财贸职业学院电子采购平台

南开大学	https://zbb.nankai.edu.cn		南开大学招投标管理办公室

天津大学	http://zbb.tju.edu.cn		天津大学招标信息网

天津工业大学	https://gzc.tjpu.edu.cn		天津工业大学国有资产与设备管理处

天津工业大学	https://gzc.tiangong.edu.cn		天津工业大学国有资产与设备管理处

天津理工大学	http://jgz.tjut.edu.cn		天津理工大学资产管理处

天津中医药大学	https://xinxi.tjutcm.edu.cn		天津中医药大学信息公开网

天津师范大学	http://cgb.tjnu.edu.cn		天津师范大学招标采购管理办公室

天津师范大学	http://hqglc.tjnu.edu.cn		天津师范大学后勤管理处

天津职业技术师范大学	https://sbc.tute.edu.cn	  国有资产管理处

天津商业大学	http://zbb.tjcu.edu.cn		天津商业大学招投标管理工作办公室

天津财经大学	http://zcc.tjufe.edu.cn		天津财经大学-资产管理处

天津城建大学	http://guozichu.tcu.edu.cn		天津城建大学国有资产管理处

天津中德应用技术大学	https://zcgl.tsguas.edu.cn 天津中德大学资产管理处

河北工程大学	http://zhaobiao.hebeu.edu.cn		河北工程大学招投标信息网

河北工业大学	https://zbcg.hebut.edu.cn		河北工业大学招标采购中心

河北农业大学	http://zhbcg.hebau.edu.cn		河北农业大学招投标信息管理系统

承德医学院	http://gzc.cdmc.edu.cn		承德医学院国资管理处

保定学院	http://gzc.bdu.edu.cn		保定学院国资处

唐山师范学院	https://gzc.tstc.edu.cn		唐山师范学院国资处

衡水学院	https://ztb.hsnc.edu.cn		衡水学院招投标信息网

石家庄学院	https://gzc.sjzc.edu.cn		石家庄学院国资处

石家庄铁道大学	http://gzc.stdu.edu.cn		石家庄铁道大学国有资产管理处

石家庄铁道大学	http://hqc.stdu.edu.cn		石家庄铁道大学后勤管理处

燕山大学	http://zcglc.ysu.edu.cn		燕山大学实验室与资产管理处

燕山大学	http://jwc.ysu.edu.cn		燕山大学教务处

唐山学院	http://gzc.tsc.edu.cn		唐山学院国资处

北华航天工业学院	https://gzc.nciae.edu.cn		北华航天工业学院国资处

防灾科技学院	https://zcglc.cidp.edu.cn		防灾科技学院资产管理处

河北工程技术学院	http://zhaobiao.hebeu.edu.cn		河北工程大学招投标信息网

华北电力大学科技学院	https://zb.ncepu.edu.cn		华北电力大学招标中心

北京交通大学海滨学院	http://cg.bjtuhbxy.edu.cn		学院采购中心

太原科技大学	https://syc.tyust.edu.cn		太原科技大学资产管理处

中北大学	http://jbjsc.nuc.edu.cn		中北大学基本建设处

太原理工大学	http://zbcg.tyut.edu.cn		太原理工大学招标与采购管理系统

太原理工大学	http://cgzb.tyut.edu.cn		太原理工大学招标与采购中心

太原理工大学	http://gzc.tyut.edu.cn		太原理工大学国有资产管理处

山西农业大学	http://zbb.sxau.edu.cn		山西农业大学招标办公室

山西农业大学	http://hqc.sxau.edu.cn		山西农业大学后勤管理处

太原师范学院	http://zcglc.tynu.edu.cn		太原师范学院资产管理处

长治学院	https://zcc.czc.edu.cn		长治学院资产管理处

忻州师范学院	http://zcgl.xztu.edu.cn		忻州师范学院资产管理处

吕梁学院	http://gyzcc.llhc.edu.cn		吕梁学院国有资产管理处

太原工业学院	https://zcc.tit.edu.cn		太原工业学院国有资产处

内蒙古科技大学	https://uaao.imust.edu.cn		内蒙古科技大学国资处

内蒙古工业大学	http://zbcg.imut.edu.cn		内蒙古工业大学招标采购中心

内蒙古农业大学	https://gzc.imau.edu.cn		内蒙古农业大学国有资产管理处

内蒙古农业大学	https://hqc.imau.edu.cn		内蒙古农业大学后勤管理处

鄂尔多斯应用技术学院	https://amd.oit.edu.cn	学院资产管理处

鄂尔多斯应用技术学院	https://hqc.oit.edu.cn		学院后勤管理处

辽宁大学	http://zbcg.lnu.edu.cn		辽宁大学招标采购办公室

复旦大学	https://czzx.fudan.edu.cn		复旦大学采购与招标管理中心

同济大学	https://czb.tongji.edu.cn		同济大学采购与招标网

上海交通大学	http://zcb.sjtu.edu.cn		上海交通大学数字化采购平台

华东理工大学	http://czzx.ecust.edu.cn		华东理工大学采购招标网

上海理工大学	https://zbb.usst.edu.cn		上海理工大学招标办公室

东华大学	http://ztb.dhu.edu.cn		东华大学采购与招投标管理中心网站

东华大学	http://zcdep.dhu.edu.cn		东华大学资产管理处

上海师范大学	http://zcc.shnu.edu.cn		上海师范大学资产与实验室管理处

上海财经大学	https://zhaobiao.sufe.edu.cn		上海财经大学采购与招标网

上海戏剧学院	https://zt.sta.edu.cn		上海戏剧学院采购与招投标管理中心

上海大学	https://sbc.shu.edu.cn		上海大学实验室与设备管理处

上海大学	https://czb.shu.edu.cn		上海大学采购与招标管理办公室

上海立信会计金融学院	https://zb.lixin.edu.cn		学院招标管理与法务中心

上海杉达学院	https://jijian.sandau.edu.cn		上海杉达学院基建处

上海第二工业大学	http://zc.sspu.edu.cn		资产与实验室管理处

上海师范大学天华学院	https://zcsbc.sthu.edu.cn	学院资产设备处

上海农林职业技术学院	http://zbb.jsafc.edu.cn		学院招投标办公室

南京大学	https://zb.nju.edu.cn		南京大学招标与采购管理系统

南京大学	https://ndzbb.nju.edu.cn		南京大学招标办公室

苏州大学	http://zbzx.suda.edu.cn		苏州大学采购与招投标管理中心

苏州大学	http://gzc.suda.edu.cn		苏州大学国有资产与实验室管理处

东南大学	https://dnzb.seu.edu.cn		东南大学招投标管理办公室

南京航空航天大学	http://cg.nuaa.edu.cn		南京航空航天大学采购一体化平台

南京航空航天大学	http://gzc.nuaa.edu.cn		南京航空航天大学国资处

南京理工大学	http://zbpt.njust.edu.cn		南京理工大学招投标办公室

江苏科技大学	https://zbb.just.edu.cn		江苏科技大学招投标工作办公室

中国矿业大学	http://zhbb.cumt.edu.cn		中国矿业大学招投标办公室

南京工业大学	http://ztb.njtech.edu.cn		南京工业大学招投标管理中心

常州大学	http://zb.cczu.edu.cn		常州大学采购与招投标办公室

南京邮电大学	http://zbb.njupt.edu.cn		南京邮电大学采购招标管理办公室

河海大学	http://zbb.hhu.edu.cn		河海大学招投标办公室

江南大学	http://zhaobiao.jiangnan.edu.cn		江南大学采购与招标信息网

江南大学	http://nic.jiangnan.edu.cn		江南大学信息化信息化建设与管理中心

江苏大学	https://cgb.ujs.edu.cn		江苏大学采购与招标办公室

江苏大学	https://gzc.ujs.edu.cn		江苏大学国有资产管理处

南京信息工程大学	https://zbc.nuist.edu.cn		南京信息工程大学招标信息网

南通大学	https://ztb.ntu.edu.cn		南通大学招投标办公室-服务类

盐城工学院	http://zbxx.ycit.edu.cn		盐城工学院招投标管理

南京农业大学	http://zbb.njau.edu.cn		南京农业大学招标采购网 - 货物类项目

南京农业大学	http://nc.njau.edu.cn		南京农业大学新校区建设指挥部

南京医科大学	http://zcc.njmu.edu.cn		南京医科大学资产管理处

南京医科大学	http://sjfwc.njmu.edu.cn		南京医科大学审计与法务处

南京中医药大学	https://zbb.njucm.edu.cn		南京中医药大学采购信息网

中国药科大学	http://cgzx.cpu.edu.cn		中国药科大学招投标办公室-货物类

南京师范大学	http://zcc.njnu.edu.cn		南京师范大学资产管理处

江苏师范大学	http://ztbb.jsnu.edu.cn		江苏师范大学招投标办公室

淮阴师范学院	http://ztb.hytc.edu.cn		淮阴师范学院招标采购网

淮阴师范学院	http://hqglc.hytc.edu.cn	国有资产管理办公室

南京财经大学	http://gzc.nufe.edu.cn		南京财经大学国有资产管理处

南京财经大学	http://zcjy.nufe.edu.cn		南京财经大学资产经营有限公司

苏州科技大学	http://sbzb.usts.edu.cn		苏州科技大学招投标管理办公室

大连理工大学	http://cgzb.dlut.edu.cn		大连理工大学采购与招标管理办公室

大连理工大学	http://cgbmis.dlut.edu.cn		大连理工大学采购信息网

沈阳航空航天大学	https://gzc.sau.edu.cn		沈阳航空航天大学国资处

沈阳航空航天大学	https://dzb.sau.edu.cn		沈阳航空航天大学党政办公室

东北大学	http://www.czzx.neu.edu.cn		东北大学采购与招标管理中心

辽宁工程技术大学	http://gzc.lntu.edu.cn		辽宁工程技术大学国有资产管理处

大连交通大学	http://zc.djtu.edu.cn		大连交通大学招标采购中心

大连工业大学	http://wzsb.dep.dlpu.edu.cn		大连工业大学国有资产管理处

沈阳建筑大学	http://cgzb.sjzu.edu.cn		沈阳建筑大学采购招标办公室

大连医科大学	https://zbcgzx.dmu.edu.cn		大连医科大学招标采购中心

沈阳医学院	https://gyzcglc.symc.edu.cn		沈阳医学院国有资产管理处

辽宁师范大学	https://cgzb.lnnu.edu.cn		辽宁师范大学采购与招标管理办公室

辽宁师范大学	https://hqjt.lnnu.edu.cn		辽宁师范大学后勤服务产业集团

沈阳师范大学	https://zcglc.synu.edu.cn		沈阳师范大学资产管理处

大连外国语大学	https://czzx.dlufl.edu.cn		大连外国语大学采购与招标管理中心

沈阳体育学院	http://hq.syty.edu.cn		沈阳体育学院国有资产管理中心

沈阳工程学院	http://zcglc.sie.edu.cn		沈阳工程学院资产管理处

东北师范大学	http://zbcg.nenu.edu.cn		东北师大招标管理中心

长春师范大学	http://zbcg.ccsfu.edu.cn		长春师范大学-招标采购中心

吉林外国语大学	https://czzx.dlufl.edu.cn		大连外国语大学采购管理中心

长春工程学院	http://zcc.ccit.edu.cn		长春工程学院资产处

长春大学	https://zbcgzx.ccu.edu.cn		长春大学招标采购中心

长春财经学院	https://assets.ccufe.edu.cn		资产采购中心

东北师范大学人文学院	http://zcgl.chsnenu.edu.cn		学院资产管理处

黑龙江大学	http://gzc.hlju.edu.cn		黑龙江大学国有资产管理处

哈尔滨工业大学	http://gzc.hit.edu.cn		哈尔滨工业大学招标与采购管理中心

哈尔滨理工大学	http://gzc.hrbust.edu.cn		哈尔滨理工大学国资处

哈尔滨工程大学	http://heuzczx.hrbeu.edu.cn		哈尔滨工程大学采购中心

黑龙江科技大学	http://zcglc.usth.edu.cn		黑龙江科技大学资产管理处

东北石油大学	http://cgxxw.nepu.edu.cn		东北石油大学采购信息网

东北农业大学	http://gzc.neau.edu.cn		东北农业大学国有资产管理处

东北林业大学	http://zbb.nefu.edu.cn		东北林业大学采购与招标管理中心

哈尔滨金融学院	https://zcglc.hrbfu.edu.cn		哈尔滨金融学院国有资产管理处

吉林大学	https://cgglzx.jlu.edu.cn		吉林大学招标与采购管理中心

延边大学	http://cg.ybu.edu.cn		延边大学采购网

吉林农业大学	https://cgzx.jlau.edu.cn		吉林农业大学采购与招投标管理中心

长春中医药大学	https://gzc.ccucm.edu.cn		长春中医药大学国有资产管理处

