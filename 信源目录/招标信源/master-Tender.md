## 主流招标信源站点

### 更新日期：2022-3-15 

如有任何问题请联系经理，

 **微信号： techflag | 电话： 13505146123** 

<br><br>


1.国家公共资源平台  http://deal.ggzy.gov.cn/ds/deal/dealList.jsp?HEADER_DEAL_TYPE=01

2.中央政府采购平台·中央公告  http://www.ccgp.gov.cn/cggg/zygg/

3.中央政府采购平台·地方公告  http://www.ccgp.gov.cn/cggg/dfgg/

4.中国政府采购网【省市】http://www.zycg.gov.cn/freecms/site/zygjjgzfcgzx/cggg/index.html

5.医和网	http://jyzc.yihewanggpo.com/index.html

6.北京市投资项目在线审批监管平台	http://tzxm.beijing.gov.cn/

7.部队采购网	https://www.plap.cn/

8.教育部政府采购网	http://www.zfcg.edu.cn/

9.中国招标投标公共服务平台	http://bulletin.cebpubservice.com/

10.国家公共资源网	http://www.ggzy.gov.cn/

11.中招联和	http://www.365trade.com.cn

12.中国中央政府采购网	http://www.ccgp.gov.cn

13.江苏省招标投标公共服务平台	https://jszbtb.com/

14.中央国家机关政府采购中心(电子卖场)	http://mkt.zycg.gov.cn/

15.武汉市建设工程招标投标交易平台	http://www.jy.whzbtb.com/

16.江苏招标	http://www.jszb.com.cn/jszb/

17.机电产品招标投标电子交易平台	https://www.chinabidding.com/bid/info/search.htm

18.安徽省招标集团股份有限公司	http://www.ah-inter.com/

19.比比网电子招标投标交易平台	http://www.bitbid.cn/

20.彩云电子招标采购平台	http://www.caiyunzb.com/

21.东风招投标交易中心	http://jyzx.dfmbidding.com/

22.甘肃省政府投资项目电子招投标交易平台	http://www.gsebidding.com/

23.国信阳光电子招投标平台	http://www.e-bidding.org/

24.国义招标采购平台	http://www.ebidding.com/

25.河南阳光电子招标采购交易平台	http://www.sunbidding.com/

26.湖北省公共资源电子交易平台	http://www.hbggzy.cn/

27.江苏海外招标业务管理系统	http://www.jociec.com/

28.江苏省机电设备招标投标交易平台	http://www.szyc99.cn/index/transaction

29.洛阳市服务外包公共信息平台	http://www.lyos.com.cn/

30.南京市货物招标投标监督平台	http://www.njhwzbb.com/Supervision/SupervisionLogin

31.山西伟拓电子招标投标交易平台	http://www.sxdzzb.com/

32.陕西诚信达	http://bidshanxiarea.youliannet.com/

33.台州公共资源交易中心网  http://www.tzztb.com/

34.武钢电子招标投标平台  http://www.wiscobidding.com.cn/

35.西南项管电子招标投标系统	http://sspmc.ebidding.net.cn/

36.西招国际电子招标投标系统	http://xzitc.ebidding.net.cn/

37.阳光高采	http://www.ebidding.cn/

38.阳光云采	http://www.etrading.cn/

39.优质采电子交易平台	http://www.youzhicai.com/

40.招采进宝平台	http://www.zcjb.com.cn/

41.招天下全流程电子招标采购平台	http://www.zhaotx.cn/

42.中国电力设备信息网电子商务平台	http://www.cpeinet.com.cn/

43.中国石化电子招标投标交易网	http://ebidding.sinopec.com/

44.中国西部公共资源交易网	http://www.westebidding.com/

45.中海建公共资源电子招投标交易网	http://www.coc-ebidding.com/ZHJ_Web/

46.中航招标网	http://bid.aited.cn/

47.中煤招标网	http://www.zmzb.com/

48.山西比比网	http://www.bitbid.cn/

49.中国建设招标网	https://www.jszhaobiao.com/

50.国义招标股份有限公司	https://www.gmgitc.com/

51.中国电力采购与招标网	http://www.chinabiddingzb.com/

52.中国移动采购与招标网	https://b2b.10086.cn/b2b/main/preIndex.html

53.四川日报招标比选网	www.sczbbx.com

54.河南招标采购综合网	http://hnzbcg.cn/

55.中国铁路集团招标网站	http://www.ztjs.net.cn/col/col3602/index.html

56.中国冶金科工集团有限公司采购中心	https://ec.mcc.com.cn/logonAction.do#

57.中国核工业建设股份有限公司	https://gys.cnecc.com/portal/list.do?chnlcode=result

58.云筑网	https://www.yzw.cn/

59.解放号	www.jfh.com

60.国机集团	http://epp.sinomach.com.cn/IndexViewController.do?method=index

61.政采云	https://inquiryhall.zcygov.cn/result?utm=a0017.b0054.cl5.11.a4b40d40e16211eb92a61919944c9dd1

62.烟草行业招标网站	http://www.tobaccobid.com/

63.内蒙古招标服务平台	http://www.nmgztb.com.cn/

64.中国招标公共服务平台	https://bulletin.cebpubservice.com/