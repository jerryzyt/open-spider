package com.stonedt.spider.service;

import com.stonedt.spider.dao.WebSiteTypeDao;
import org.springframework.beans.factory.annotation.Autowired;

public interface StatisticService {

   boolean findEsCount();

}
