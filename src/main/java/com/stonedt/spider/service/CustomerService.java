package com.stonedt.spider.service;

import java.util.List;

import com.stonedt.spider.entity.CustomerEntity;

public interface CustomerService {
	
	List<CustomerEntity> getList();
}
