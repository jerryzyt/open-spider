package com.stonedt.spider.util;

/**
 * 常量类
 * @author MaPeng
 *
 */
public interface Constance{

	
	public static final String EDZDLH = "重大利好";
	public static final String EDLH = "利好";
	public static final String EDLZ = "利中";
	public static final String EDLK = "利空";
	public static final String EDZDLK = "重大利空";
	public static final Integer PAGE_SIZE = 15;
	public static final Integer PAGE_NUM = 10;
	public static final Integer PAGE_SIZE10 = 10;
	public static final String DPATH="/opt/NLP/stonedtnlp/";
}
