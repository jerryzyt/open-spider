package com.stonedt.spider.entity;

public class KeyWord {
	
	private int id;//编号
	private String keyword;//关键词
	private String createtime;//创建时间
	private int handle_flag;//处理标记
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public int getHandle_flag() {
		return handle_flag;
	}
	public void setHandle_flag(int handle_flag) {
		this.handle_flag = handle_flag;
	}
	
	
	
	

}
