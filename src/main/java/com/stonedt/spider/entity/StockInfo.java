package com.stonedt.spider.entity;

import java.io.Serializable;

public class StockInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id; 
	private String name;  
	private String code;  
	private String data_one; 
	private String data_two; 
	private String data_three;  
	private String openPrice;
	private String highestPrice;
	private String lowestPrice; 
	private String epsttm;
	private String pettm; 
	private String pe; 
	private String totalEquity; 
	private String marketValue; 
	private String preClosePrice;  
	private String latestYearMax;  
	private String latestYearMin;  
	private String bps;
	private String pb; 
	private String negEquity;
	private String negMarketValue; 
	private String kjson;
	private String coreTheme;
	private String senior;
	private String gudong;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getData_one() {
		return data_one;
	}
	public void setData_one(String data_one) {
		this.data_one = data_one;
	}
	public String getData_two() {
		return data_two;
	}
	public void setData_two(String data_two) {
		this.data_two = data_two;
	}
	public String getData_three() {
		return data_three;
	}
	public void setData_three(String data_three) {
		this.data_three = data_three;
	}
	public String getOpenPrice() {
		return openPrice;
	}
	public void setOpenPrice(String openPrice) {
		this.openPrice = openPrice;
	}
	public String getHighestPrice() {
		return highestPrice;
	}
	public void setHighestPrice(String highestPrice) {
		this.highestPrice = highestPrice;
	}
	public String getLowestPrice() {
		return lowestPrice;
	}
	public void setLowestPrice(String lowestPrice) {
		this.lowestPrice = lowestPrice;
	}
	public String getEpsttm() {
		return epsttm;
	}
	public void setEpsttm(String epsttm) {
		this.epsttm = epsttm;
	}
	public String getPettm() {
		return pettm;
	}
	public void setPettm(String pettm) {
		this.pettm = pettm;
	}
	public String getPe() {
		return pe;
	}
	public void setPe(String pe) {
		this.pe = pe;
	}
	public String getTotalEquity() {
		return totalEquity;
	}
	public void setTotalEquity(String totalEquity) {
		this.totalEquity = totalEquity;
	}
	public String getMarketValue() {
		return marketValue;
	}
	public void setMarketValue(String marketValue) {
		this.marketValue = marketValue;
	}
	public String getPreClosePrice() {
		return preClosePrice;
	}
	public void setPreClosePrice(String preClosePrice) {
		this.preClosePrice = preClosePrice;
	}
	public String getLatestYearMax() {
		return latestYearMax;
	}
	public void setLatestYearMax(String latestYearMax) {
		this.latestYearMax = latestYearMax;
	}
	public String getLatestYearMin() {
		return latestYearMin;
	}
	public void setLatestYearMin(String latestYearMin) {
		this.latestYearMin = latestYearMin;
	}
	public String getBps() {
		return bps;
	}
	public void setBps(String bps) {
		this.bps = bps;
	}
	public String getPb() {
		return pb;
	}
	public void setPb(String pb) {
		this.pb = pb;
	}
	public String getNegEquity() {
		return negEquity;
	}
	public void setNegEquity(String negEquity) {
		this.negEquity = negEquity;
	}
	public String getNegMarketValue() {
		return negMarketValue;
	}
	public void setNegMarketValue(String negMarketValue) {
		this.negMarketValue = negMarketValue;
	}
	public String getKjson() {
		return kjson;
	}
	public void setKjson(String kjson) {
		this.kjson = kjson;
	}
	public String getCoreTheme() {
		return coreTheme;
	}
	public void setCoreTheme(String coreTheme) {
		this.coreTheme = coreTheme;
	}
	public String getSenior() {
		return senior;
	}
	public void setSenior(String senior) {
		this.senior = senior;
	}
	public String getGudong() {
		return gudong;
	}
	public void setGudong(String gudong) {
		this.gudong = gudong;
	}
	
	
	
}
