package com.stonedt.spider.entity;

import lombok.Data;

@Data
public class EsIndexRatio {
    private String esIndex;
    private String name;
    private double value;
}
