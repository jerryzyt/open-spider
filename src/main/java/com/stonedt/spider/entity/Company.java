package com.stonedt.spider.entity;

public class Company {
	private String id;
	private String firm_id;//'公司id',
	private String name;//'企业名称',
	private String abbr;//
	private String legal_representative;//'法定代表人',
	private String status;//经营状态',
	private String establish_time;//成立日期',
	private String registered_capital_str;//注册资本',
	private String reality_capital_str;//'实缴资本',
	private String registered_capital;//'注册资本',
	private String reality_capital;//'实缴资本',
	private String approval_date;//'核准日期',
	private String companyInfo;//'公司简介',
	private String uniform_social_credit_code;//'统一社会信用代码',
	private String organization_code;//'组织机构代码',
	private String registration_number;//'工商注册号',
	private String taxpayer_identification_number;//'纳税人识别号',
	private String industry_involved;//'所属行业',
	private String enterprise_type;//'企业类型',
	private String business_term;//'营业期限',
	private String register_organization;//'登记机关',
	private String address;//'企业地址',
	private String register_adress;//'注册地址',
	private String business_scope;//'经营范围',
	private String website;//'官网',
	private String province;//'省',
	private String city;//'市',
	private String type;//'是否补全',
	private String is_completed;//'是否补全',
}
