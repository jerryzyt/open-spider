package com.stonedt.spider.entity;

import java.util.Date;

public class SpiderSeedConfigUtilOb {
	private Integer id;
	private String seed_util_name;
	private String seed_title_config;
	private String seed_date_config;
	private String seed_text_config;
	private Integer seed_text_config_type;
	private String seed_interval_config;
	private Integer seed_thread_config;
	private Integer website_id;
	private Integer create_user_id;
	private Date create_date;
	private String seed_origin_config;
	private Integer seed_sleep_config;
	private String seed_linkurl_config;
	private String seed_origin_url_config;
	private Integer seed_spider_type;
	private Integer other_seed_id;
	private Integer other_website_id;
	private String url_reg;
	private String seed_type;
	private String seed_list_url;
	private String seed_author_config;
	private String seed_author_url_config;
	private String seed_author_avatar_config;
	private String seed_page_rule_config;
	private String seed_url_config;
	
	
	
	public String getSeed_url_config() {
		return seed_url_config;
	}
	public void setSeed_url_config(String seed_url_config) {
		this.seed_url_config = seed_url_config;
	}
	public String getSeed_type() {
		return seed_type;
	}
	public void setSeed_type(String seed_type) {
		this.seed_type = seed_type;
	}
	public String getSeed_list_url() {
		return seed_list_url;
	}
	public void setSeed_list_url(String seed_list_url) {
		this.seed_list_url = seed_list_url;
	}
	public String getSeed_author_config() {
		return seed_author_config;
	}
	public void setSeed_author_config(String seed_author_config) {
		this.seed_author_config = seed_author_config;
	}
	public String getSeed_author_url_config() {
		return seed_author_url_config;
	}
	public void setSeed_author_url_config(String seed_author_url_config) {
		this.seed_author_url_config = seed_author_url_config;
	}
	public String getSeed_author_avatar_config() {
		return seed_author_avatar_config;
	}
	public void setSeed_author_avatar_config(String seed_author_avatar_config) {
		this.seed_author_avatar_config = seed_author_avatar_config;
	}
	public String getSeed_page_rule_config() {
		return seed_page_rule_config;
	}
	public void setSeed_page_rule_config(String seed_page_rule_config) {
		this.seed_page_rule_config = seed_page_rule_config;
	}
	public String getUrl_reg() {
		return url_reg;
	}
	public void setUrl_reg(String url_reg) {
		this.url_reg = url_reg;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSeed_util_name() {
		return seed_util_name;
	}
	public void setSeed_util_name(String seed_util_name) {
		this.seed_util_name = seed_util_name;
	}
	public String getSeed_title_config() {
		return seed_title_config;
	}
	public void setSeed_title_config(String seed_title_config) {
		this.seed_title_config = seed_title_config;
	}
	public String getSeed_date_config() {
		return seed_date_config;
	}
	public void setSeed_date_config(String seed_date_config) {
		this.seed_date_config = seed_date_config;
	}
	public String getSeed_text_config() {
		return seed_text_config;
	}
	public void setSeed_text_config(String seed_text_config) {
		this.seed_text_config = seed_text_config;
	}
	public Integer getSeed_text_config_type() {
		return seed_text_config_type;
	}
	public void setSeed_text_config_type(Integer seed_text_config_type) {
		this.seed_text_config_type = seed_text_config_type;
	}
	public String getSeed_interval_config() {
		return seed_interval_config;
	}
	public void setSeed_interval_config(String seed_interval_config) {
		this.seed_interval_config = seed_interval_config;
	}
	public Integer getSeed_thread_config() {
		return seed_thread_config;
	}
	public void setSeed_thread_config(Integer seed_thread_config) {
		this.seed_thread_config = seed_thread_config;
	}
	public Integer getWebsite_id() {
		return website_id;
	}
	public void setWebsite_id(Integer website_id) {
		this.website_id = website_id;
	}
	public Integer getCreate_user_id() {
		return create_user_id;
	}
	public void setCreate_user_id(Integer create_user_id) {
		this.create_user_id = create_user_id;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getSeed_origin_config() {
		return seed_origin_config;
	}
	public void setSeed_origin_config(String seed_origin_config) {
		this.seed_origin_config = seed_origin_config;
	}
	public Integer getSeed_sleep_config() {
		return seed_sleep_config;
	}
	public void setSeed_sleep_config(Integer seed_sleep_config) {
		this.seed_sleep_config = seed_sleep_config;
	}
	public String getSeed_linkurl_config() {
		return seed_linkurl_config;
	}
	public void setSeed_linkurl_config(String seed_linkurl_config) {
		this.seed_linkurl_config = seed_linkurl_config;
	}
	public String getSeed_origin_url_config() {
		return seed_origin_url_config;
	}
	public void setSeed_origin_url_config(String seed_origin_url_config) {
		this.seed_origin_url_config = seed_origin_url_config;
	}
	public Integer getSeed_spider_type() {
		return seed_spider_type;
	}
	public void setSeed_spider_type(Integer seed_spider_type) {
		this.seed_spider_type = seed_spider_type;
	}
	public Integer getOther_seed_id() {
		return other_seed_id;
	}
	public void setOther_seed_id(Integer other_seed_id) {
		this.other_seed_id = other_seed_id;
	}
	public Integer getOther_website_id() {
		return other_website_id;
	}
	public void setOther_website_id(Integer other_website_id) {
		this.other_website_id = other_website_id;
	}
	@Override
	public String toString() {
		return "SpiderSeedConfigUtilOb [id=" + id + ", seed_util_name=" + seed_util_name + ", seed_title_config="
				+ seed_title_config + ", seed_date_config=" + seed_date_config + ", seed_text_config="
				+ seed_text_config + ", seed_text_config_type=" + seed_text_config_type + ", seed_interval_config="
				+ seed_interval_config + ", seed_thread_config=" + seed_thread_config + ", website_id=" + website_id
				+ ", create_user_id=" + create_user_id + ", create_date=" + create_date + ", seed_origin_config="
				+ seed_origin_config + ", seed_sleep_config=" + seed_sleep_config + ", seed_linkurl_config="
				+ seed_linkurl_config + ", seed_origin_url_config=" + seed_origin_url_config + ", seed_spider_type="
				+ seed_spider_type + ", other_seed_id=" + other_seed_id + ", other_website_id=" + other_website_id
				+ ", url_reg=" + url_reg + ", seed_type=" + seed_type + ", seed_list_url=" + seed_list_url
				+ ", seed_author_config=" + seed_author_config + ", seed_author_url_config=" + seed_author_url_config
				+ ", seed_author_avatar_config=" + seed_author_avatar_config + ", seed_page_rule_config="
				+ seed_page_rule_config + ", seed_url_config=" + seed_url_config + "]";
	}
	
	
	
}
