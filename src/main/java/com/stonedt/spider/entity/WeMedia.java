package com.stonedt.spider.entity;

public class WeMedia {
	  private String id;
	  private String name;
	  private String logo;
	  private String slogan;
	  private String remark;
	  private String platform_id;
	  private String home_page_url;
	  private String certification_instructions;
	  private String production_count;
	  private String focus_count;
	  private String reader_count;
	  private String spider_level;
	  private String platform_name;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getSlogan() {
		return slogan;
	}
	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPlatform_id() {
		return platform_id;
	}
	public void setPlatform_id(String platform_id) {
		this.platform_id = platform_id;
	}
	public String getHome_page_url() {
		return home_page_url;
	}
	public void setHome_page_url(String home_page_url) {
		this.home_page_url = home_page_url;
	}
	public String getCertification_instructions() {
		return certification_instructions;
	}
	public void setCertification_instructions(String certification_instructions) {
		this.certification_instructions = certification_instructions;
	}
	public String getProduction_count() {
		return production_count;
	}
	public void setProduction_count(String production_count) {
		this.production_count = production_count;
	}
	public String getFocus_count() {
		return focus_count;
	}
	public void setFocus_count(String focus_count) {
		this.focus_count = focus_count;
	}
	public String getReader_count() {
		return reader_count;
	}
	public void setReader_count(String reader_count) {
		this.reader_count = reader_count;
	}
	public String getSpider_level() {
		return spider_level;
	}
	public void setSpider_level(String spider_level) {
		this.spider_level = spider_level;
	}
	public String getPlatform_name() {
		return platform_name;
	}
	public void setPlatform_name(String platform_name) {
		this.platform_name = platform_name;
	}
	@Override
	public String toString() {
		return "WeMedia [id=" + id + ", name=" + name + ", logo=" + logo + ", slogan=" + slogan + ", remark=" + remark
				+ ", platform_id=" + platform_id + ", home_page_url=" + home_page_url + ", certification_instructions="
				+ certification_instructions + ", production_count=" + production_count + ", focus_count=" + focus_count
				+ ", reader_count=" + reader_count + ", spider_level=" + spider_level + ", platform_name="
				+ platform_name + "]";
	}
	  
	  
	  
	  

}
