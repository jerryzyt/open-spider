package com.stonedt.spider.entity;

import java.util.Date;

public class SeedCount {
	private Integer seedid;
	private Date date;
	private Integer count;
	public Integer getSeedid() {
		return seedid;
	}
	public void setSeedid(Integer seedid) {
		this.seedid = seedid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	

}
