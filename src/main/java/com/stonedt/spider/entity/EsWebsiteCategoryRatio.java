package com.stonedt.spider.entity;

import lombok.Data;

@Data
public class EsWebsiteCategoryRatio {
    private int id;
    private String name;
    private double value;
}
