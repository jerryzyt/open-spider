package com.stonedt.spider.entity;

import java.io.Serializable;

public class StockFinancial implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id; 
	private String code;  
	private String cwzy_json;
	private String zcfz_json;
	private String lr_json;  
	private String xjll_json;
	private String zygc_json;
	private String mgzb_json;
	private String ylnl_json;
	private String zjjg_json;
	private String yynl_json;
	private String cznl_json;
	private String zb_xjll_json;
	private String cwbb_json;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCwzy_json() {
		return cwzy_json;
	}
	public void setCwzy_json(String cwzy_json) {
		this.cwzy_json = cwzy_json;
	}
	public String getZcfz_json() {
		return zcfz_json;
	}
	public void setZcfz_json(String zcfz_json) {
		this.zcfz_json = zcfz_json;
	}
	public String getLr_json() {
		return lr_json;
	}
	public void setLr_json(String lr_json) {
		this.lr_json = lr_json;
	}
	public String getXjll_json() {
		return xjll_json;
	}
	public void setXjll_json(String xjll_json) {
		this.xjll_json = xjll_json;
	}
	public String getZygc_json() {
		return zygc_json;
	}
	public void setZygc_json(String zygc_json) {
		this.zygc_json = zygc_json;
	}
	public String getMgzb_json() {
		return mgzb_json;
	}
	public void setMgzb_json(String mgzb_json) {
		this.mgzb_json = mgzb_json;
	}
	public String getYlnl_json() {
		return ylnl_json;
	}
	public void setYlnl_json(String ylnl_json) {
		this.ylnl_json = ylnl_json;
	}
	public String getZjjg_json() {
		return zjjg_json;
	}
	public void setZjjg_json(String zjjg_json) {
		this.zjjg_json = zjjg_json;
	}
	public String getYynl_json() {
		return yynl_json;
	}
	public void setYynl_json(String yynl_json) {
		this.yynl_json = yynl_json;
	}
	public String getCznl_json() {
		return cznl_json;
	}
	public void setCznl_json(String cznl_json) {
		this.cznl_json = cznl_json;
	}
	public String getZb_xjll_json() {
		return zb_xjll_json;
	}
	public void setZb_xjll_json(String zb_xjll_json) {
		this.zb_xjll_json = zb_xjll_json;
	}
	public String getCwbb_json() {
		return cwbb_json;
	}
	public void setCwbb_json(String cwbb_json) {
		this.cwbb_json = cwbb_json;
	}
	
	

}
