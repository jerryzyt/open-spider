package com.stonedt.spider.entity;

import lombok.Data;

@Data
public class MinioFileCount {
    private int count;
    private int size;
}
