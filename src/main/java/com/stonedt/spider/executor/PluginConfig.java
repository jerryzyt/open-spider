package com.stonedt.spider.executor;

import com.stonedt.spider.model.Plugin;

public interface PluginConfig {
	
	Plugin plugin();
	
}