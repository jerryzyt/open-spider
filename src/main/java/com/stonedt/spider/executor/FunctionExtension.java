package com.stonedt.spider.executor;

public interface FunctionExtension {
	
	Class<?> support();
}
